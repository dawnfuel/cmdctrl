class Config(object):
    DEBUG = False
    ACCOUNT_SID = "ACbab21f4548e3945ff6830eb9c23281eb"
    AUTH_TOKEN = "798604d19415f5ef6490fd2eb82f36fa"
    DASHBOARD_NODE = "dashboard.grit.systems:9092"
    SQLALCHEMY_POOL_SIZE = 100
    SQLALCHEMY_MAX_OVERFLOW = 250
    CONN_MAX_AGE = 0


class Development(Config):
    DEBUG = True
    MONGO_DATABASE = 'mongodb://localhost:27017/'
    #MONGO_DATABASE = 'mongodb://dashboard.grit.systems:27017/'
    KAFKA_NODE = 'localhost:9092'
    POSTGRES_DATABASE = 'postgresql://postgres:postgres@localhost/cmdctrl'
    GROUP_ID = '_cmdctrl_development'


class Production(Config):
    MONGO_DATABASE = 'mongodb://dashboard.grit.systems:27017/'
    KAFKA_NODE = 'test.grit.systems:9092'
    POSTGRES_DATABASE = 'postgresql://cmdctrl:WTcFe@localhost/cmdctrl'
    GROUP_ID = 'group_cmdctrl'
