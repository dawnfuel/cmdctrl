# README #

### What is this repository for? ###

* Command and Control Centre for Monitoring Probes and Meters

### How do I get set up? ###

* npm install socket.io
* clone repo 
* pip install -r requirements.txt
* mongorestore database dump  
* python hello.py
* http://127.0.0.1:5000

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact