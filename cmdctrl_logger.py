#!/usr/bin/env python
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('[%(asctime)s cmdctrl] %(message)s')

file_handler = logging.FileHandler('cmdctrl.log')
file_handler.setFormatter(formatter)

# File handler for OFFLINE Events
# offline_handler = logging.FileHandler('cmdctrl_probes_OFFLINE.log')
# offline_handler.setLevel(logging.CRITICAL)
# offline_handler.setFormatter(formatter)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)

exception_handler = logging.FileHandler('cmdctrl_exceptions.log')
exception_handler.setLevel(logging.WARNING)
exception_handler.setFormatter(formatter)

logger.addHandler(file_handler)
# logger.addHandler(offline_handler)
logger.addHandler(stream_handler)
logger.addHandler(exception_handler)
