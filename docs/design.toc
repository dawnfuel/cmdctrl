\select@language {english}
\contentsline {section}{\numberline {1}Abstract}{3}{section.1}
\contentsline {section}{\numberline {2}Design considerations}{4}{section.2}
\contentsline {paragraph}{Usability :}{4}{section*.4}
\contentsline {paragraph}{Efficiency :}{4}{section*.5}
\contentsline {paragraph}{Durability :}{4}{section*.6}
\contentsline {paragraph}{Electromagnetic Compatibility :}{4}{section*.7}
\contentsline {paragraph}{Size :}{4}{section*.8}
\contentsline {paragraph}{Cost :}{4}{section*.9}
\contentsline {section}{\numberline {3}System Design}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Software Design}{5}{subsection.3.1}
