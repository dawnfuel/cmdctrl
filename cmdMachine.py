#!/usr/bin/env python
from cmdctrl_logger import logger


class CtrlStateMachine:
    def __init__(self):
        self.stateFunctions = {}
        self.startState = None
        self.endStates = []
    # create dictionary mapping state to the function handling those states

    def addState(self, state, stateFunction, endState=0):
        self.stateFunctions[state] = stateFunction
        if endState:
            self.endStates.append(state)

    # create start state
    def setStartState(self, state):
        self.startState = state

    def run(self, response):
        stateFunction = self.stateFunctions[self.startState]
        while True:
            newState, response = stateFunction(response)
            if newState in self.endStates:
                logger.info("State: {}\n".format(newState))
                break
            else:
                stateFunction = self.stateFunctions[newState]
