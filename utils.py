#!/usr/bin/env python
from datetime import timedelta
from dateutil.parser import parse
from dateutil import tz
from time import mktime

from arrow import get
from arrow.parser import ParserError


def time_difference(start_time, stop_time):
    time1 = parse(start_time)
    time2 = parse(stop_time)

    t1 = (mktime(time1.timetuple()) + time1.microsecond / 1E6)
    t2 = (mktime(time2.timetuple()) + time2.microsecond / 1E6)

    dt = timedelta(seconds=(t2 - t1))
    return dt


def reformat_time(time):
    new_time_format = get(time).format('YYYY-MM-DDTHH:mm:ss.SSSZZ')
    return new_time_format


def tz_aware_time(time):
    try:
        if type(time) == 'arrow.arrow.Arrow':
            return get(str(time)).replace(tzinfo=tz.gettz("Africa/Lagos"))
        else:
            return get(time).replace(tzinfo=tz.gettz("Africa/Lagos"))
    except ParserError:
        return time
