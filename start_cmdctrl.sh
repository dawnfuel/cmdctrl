#!/bin/bash
# es_dir=$HOME"/Applications/elasticsearch-5.3.0"
kafka_dir=$HOME"/Applications/kafka_2.11-0.9.0.0"
cmdctrl_dir=$HOME"/Documents/REPOS/cmdctrl"

# echo $es_dir
echo "Starting ..."
# echo $cmdctrl_dir

# eval sudo rm -rf "/tmp/kafka-logs/*"

# # start elasticsearch
# tab="--tab --working-directory $es_dir"
# cmd="bash -c 'bin/elasticsearch';bash"
# foo+=($tab -e "$cmd")

# start zookeeper
tab="--tab --working-directory $kafka_dir"
cmd="bash -c 'bin/zookeeper-server-start.sh config/zookeeper.properties';bash"
# cmd="bash -c bin/zookeeper-server-start.sh config/zookeeper.properties"
foo+=($tab -e "$cmd")

# start kafka
tab="--tab --working-directory $kafka_dir"
cmd="bash -c 'sleep 7 && bin/kafka-server-start.sh config/server.properties';bash"
foo+=($tab -e "$cmd")

#start producer
tab="--tab --working-directory $cmdctrl_dir"
cmd="bash -c 'sleep 15 && python producer.py';bash"
foo+=($tab -e "$cmd")

# # start Elasticsearch indexer
# tab="--tab --working-directory $cmdctrl_dir"
# cmd="bash -c 'sleep 20 && python esNew.py';bash"
# foo+=($tab -e "$cmd")


gnome-terminal "${foo[@]}"
exit 0
