#!/usr/bin/env python
from datetime import timedelta
# from dateutil.parser import parse
# from dateutil import tz
from json import loads
# import logging
import os
# from pprint import pprint
from Queue import Queue, Empty
from multiprocessing import current_process
import re
from sys import stdout
import threading
from threading import current_thread, Thread, Lock
from time import sleep
import time
from traceback import print_exc

from arrow import now, get
# from arrow.parser import ParserError
import bcrypt
# from bson import json_util
from confluent.schemaregistry.client import CachedSchemaRegistryClient
from confluent.schemaregistry.serializers import MessageSerializer
from dominate import *
from dominate.tags import *
from dominate.util import raw
from flask import Flask, render_template, redirect, url_for, request, session
from flask_environments import Environments
from flask_migrate import Migrate
from flask_socketio import SocketIO, emit
from flask_sqlalchemy import SQLAlchemy
from gevent import monkey
monkey.patch_all()

from kafka import KafkaConsumer
from pymongo import MongoClient
from twilio.rest import Client
from requests import get as rget
# from requests.exceptions import ConnectionError
import schedule

from cmdctrl_logger import logger
from cmdMachine import CtrlStateMachine
from utils import time_difference, tz_aware_time, reformat_time
# from process_model import sqldb, ProcessProbe


app = Flask(__name__, static_url_path='/static')
app.secret_key = os.urandom(12)

env = Environments(app)
env.from_object('config')

app.config['SQLALCHEMY_DATABASE_URI'] = app.config['POSTGRES_DATABASE']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

sqldb = SQLAlchemy(app)
migrate = Migrate(app, sqldb)
socketio = SocketIO(app)

test_kafkanode = str(app.config['KAFKA_NODE'])
dashboard_kafkanode = str(app.config['DASHBOARD_NODE'])
group_id = str(app.config['GROUP_ID'])
topic_power = 'IP_Message'
topic_message = 'gritServer'
topic_tamper = 'tamperReport'
topics = [topic_power, topic_message, topic_tamper]

client = MongoClient(str(app.config['MONGO_DATABASE']))
# client = MongoClient('mongodb://107.170.79.49:27017/')

db = client['lift-mongo-app']
probes = db['probes']
configurations = db['configurations']
sources = db['powersources']
consumers = db['consumer_lines']
users = db['user.users']

content_health_params = ["probe_state", "last_probe_message",
                         "last_heartbeat", "up_since", "restart_cause", "last_restart_time", "last_power", 'messages_per_hour', 'messages_per_day']
content_sqldb_params = ["state", "last_probe_message", "last_heartbeat", "on_since",
                        "last_state", "last_restart_time", "last_power", 'messages_per_hour', 'messages_per_day']
probe_keyset = probes.find_one().keys()
config_keyset = configurations.find_one().keys()
content_ignored_params = {"wifi_password", "distributionLevel", "Channels", "ConfigID_FK", "SignalThreshold", "ErrorThreshold", "wifi_ssid", "DeviceID_FK",
                          "_id", 'UserID_FKs', 'AdminID_FK', 'GeoPoint', 'UserID_FK', 'ConfigGroupID_FK', 'GroupName', 'ErrorThreshold', 'configurationUser', 'ConfigAddress'}
alternate_health_params = ["probe_state", "last_probe_message"]
alternate_sqldb_params = ["state", "last_probe_message"]
alternate_config_params = {"configurationName", "probeName"}
probe_details_postgres_params = ["PROBE STATE", "PROBE NAME", "LAST MESSAGE", "UP SINCE",
                                 "RESTART CAUSE", "LAST POWER", "TOTAL MESSAGES IN THE LAST HOUR", "TOTAL MESSAGES IN THE LAST DAY"]
probe_details_mongo_params = [
    "SCALING FACTOR CURRENT", "SCALING FACTOR VOLTAGE", "NUMBER OF CHANNELS"]
tamper_headers = ['CHANNEL STATE', 'CHANNEL NUMBER',
                  'CHANNEL SOURCE', 'TAMPER TIME']


class ProcessProbe(sqldb.Model):
    id = sqldb.Column(sqldb.Integer, primary_key=True,
                      autoincrement=True, unique=True)
    probe_id = sqldb.Column(sqldb.String(50), unique=True)
    last_probe_message = sqldb.Column(sqldb.DateTime())
    last_heartbeat = sqldb.Column(sqldb.DateTime())
    # probe_changes = sqldb.Column(sqldb.Integer())
    on_since = sqldb.Column(sqldb.DateTime())
    # create new column for message_interval. will be for
    message_interval = sqldb.Column(sqldb.Interval(second_precision=6))
    messages_per_hour = sqldb.Column(sqldb.Integer(), default=0)
    messages_per_day = sqldb.Column(sqldb.Integer(), default=0)
    state_change_count = sqldb.Column(sqldb.Integer(), default=0)
    last_power = sqldb.Column(sqldb.Float())
    last_state = sqldb.Column(sqldb.String(50))

    last_restart_time = sqldb.Column(sqldb.DateTime())
    timeout = sqldb.Column(sqldb.Integer(), default=5)
    state = sqldb.Column(sqldb.String(50))
    expected_messages_per_hour = sqldb.Column(sqldb.Integer(), default=0)
    expected_messages_per_day = sqldb.Column(sqldb.Integer(), default=0)
    avg_message_frequency = sqldb.Column(sqldb.Integer())
    avg_heartbeat_frequency = sqldb.Column(sqldb.Integer())
    tamper_report = sqldb.relationship(
        'Tamper', backref="owner", lazy='dynamic')

    def __init__(self, probe_id):
        self.probe_id = probe_id


class Tamper(sqldb.Model):
    id = sqldb.Column(sqldb.Integer, primary_key=True,
                      autoincrement=True, unique=True)
    tamper_time = sqldb.Column(sqldb.DateTime())
    channel_number = sqldb.Column(sqldb.Integer)
    channel_source = sqldb.Column(sqldb.String(50))
    channel_state = sqldb.Column(sqldb.String(50))
    process_probe_id = sqldb.Column(
        sqldb.Integer, sqldb.ForeignKey('process_probe.id'))


class User(sqldb.Model):
    id = sqldb.Column(sqldb.Integer, primary_key=True)
    email = sqldb.Column(sqldb.String)
    password = sqldb.Column(sqldb.String)

    def __init__(self, email, password):
        self.email = email
        self.password = self.set_password(password)

    def set_password(self, password):
        return bcrypt.hashpw(password, bcrypt.gensalt(14))

    def check_password(self, password):
        return bcrypt.checkpw(password.encode('utf-8'), self.password.encode('utf-8'))


def create_cmdctrl_user():
    usermail = 'cmdctrl@grit.systems'
    userpass = 'RaAcDb17'
    user = sqldb.session.query(User).filter_by(email=usermail).first()
    if not user:
        user = User(usermail, userpass)
        sqldb.session.add(user)
        sqldb.session.commit()
        sqldb.session.remove()
    else:
        logger.info('User {email} exits'.format(email=usermail))


def offline_checker(state_dict, time_dict, onsince_dict):
    time_ref = timedelta(seconds=40.000000)
    presentTime = str(now("Africa/Lagos"))

    for probe_id in state_dict.keys():

        probe_state = state_dict[probe_id]
        last_message = time_dict[probe_id]
        time_up = onsince_dict[probe_id]
        try:
            try:
                dt = time_difference(last_message, presentTime)
            except Exception:
                dt = timedelta(seconds=1.000000)
                pass
            if probe_state == 'SHUTDOWN INITIALISED' and dt > time_ref:
                newState = "BATTERY FAILURE"

                logger.info("Processing {probe} at {time}".format(
                    probe=probe_id, time=presentTime))
                logger.info("{probe}'s battery fails at {time}'".format(
                    probe=probe_id, time=presentTime))
                logger.info("State: {}\n".format(newState))

                state_dict[probe_id] = newState

                emit_event = 'battery_failure'
                card_event = 'down_state'
                emit_state = newState
                emit_id = re.sub(':', '', probe_id)
                emit_probe_time = last_message
                emit_heartbeat = presentTime
                emit_time_up = tz_aware_time(time_up)
                card_params = [card_event, emit_state,
                               emit_id, emit_probe_time]
                table_params = [emit_event, emit_state, emit_id,
                                emit_probe_time, emit_heartbeat, emit_time_up]
                detailed_params = [emit_event, emit_state,
                                   emit_id, emit_probe_time, emit_time_up]

                card_emissions(*card_params)
                table_emissions(*table_params)
                detailed_emissions(*detailed_params)

            elif probe_state != "SHUTDOWN INITIALISED" and probe_state != "BATTERY FAILURE" and dt > time_ref:
                newState = "OFFLINE"

                logger.info("Processing {probe} at {time}".format(
                    probe=probe_id, time=presentTime))
                logger.info("{probe} goes offline at {time}".format(
                    probe=probe_id, time=presentTime))
                logger.info("State: {}\n".format(newState))

                state_dict[probe_id] = newState

                emit_event = 'offline'
                card_event = 'down_state'
                emit_state = newState
                emit_id = re.sub(':', '', probe_id)
                emit_probe_time = last_message
                emit_heartbeat = presentTime
                emit_time_up = tz_aware_time(time_up)
                card_params = [card_event, emit_state,
                               emit_id, emit_probe_time]
                table_params = [emit_event, emit_state, emit_id,
                                emit_probe_time, emit_heartbeat, emit_time_up]
                detailed_params = [emit_event, emit_state,
                                   emit_id, emit_probe_time, emit_time_up]

                card_emissions(*card_params)
                table_emissions(*table_params)
                detailed_emissions(*detailed_params)

                # socketio.emit(emit_event, {'state': emit_state, 'id': emit_id, 'probe_time': emit_probe_time, 'socket_time': emit_heartbeat, 'time_up': str(emit_time_up)}, namespace='/kstream')
                # text_notifier(phone_numbers, probeId, newState, emit_probe_time)
            else:
                sleep(0.01)
                pass
        except ValueError:
            logger.exception("ValueError")
            pass
        # sqldb.session.add(probe)
        # sqldb.session.commit()
    return state_dict


def create_offline_dict(i=[0]):
    i[0] += 1
    sqldb_probes = sqldb.session.query(ProcessProbe).all()
    probe_offline_id = []
    probe_offline_state = []
    probe_offline_time = []
    probe_on_since = []
    for sqldb_probe in sqldb_probes:
        probe_offline_id.append(str(sqldb_probe.probe_id))
        probe_offline_state.append(str(sqldb_probe.state))
        probe_offline_time.append(str(sqldb_probe.last_probe_message))
        probe_on_since.append(str(sqldb_probe.on_since))

    state_dict = dict(zip(probe_offline_id, probe_offline_state))
    time_dict = dict(zip(probe_offline_id, probe_offline_time))
    onsince_dict = dict(zip(probe_offline_id, probe_on_since))
    return state_dict, time_dict, onsince_dict, i[0]


def update_sqldb_after_offline_check(state_dict):
    sqldb_probes = sqldb.session.query(ProcessProbe).all()
    # sqldb_probes = ProcessProbe.query.all()
    for probe in sqldb_probes:
        probe_id = str(probe.probe_id)
        probe.state = state_dict[probe_id]
        sqldb.session.add(probe)
    sqldb.session.commit()
    sqldb.session.remove()


def call_offline_checker():
    global next_call
    initial_state_dict, time_dict, onsince_dict, count = create_offline_dict()
    new_state_dict = offline_checker(
        initial_state_dict, time_dict, onsince_dict)
    update_sqldb_after_offline_check(new_state_dict)

    print "\n\n\n", count
    print current_thread().name, current_process().name, "\n\n\n"
    next_call = next_call + 5
    t = threading.Timer(next_call - time.time(), call_offline_checker)
    t.name = 'offline_thread'
    t.daemon = True
    t.start()


def up_since(probe_id, probe_time):
    states_down_set = {"START", "SHUTDOWN TRANSITION", "OFFLINE",
                       "PROBE OFF", "BATTERY FAILURE", "SHUTDOWN INITIALISED"}
    probe = sqldb.session.query(ProcessProbe).filter_by(
        probe_id=probe_id).first()
    # probe = ProcessProbe.query.filter_by(probe_id=probe_id).first()
    time_up = probe.on_since
    if probe.state in states_down_set:
        time_up = probe_time
    return time_up


def trans_controller(m):
    presentTime = str(now("Africa/Lagos"))
    if 'report' in m:
        newState = 'TAMPER_CHECK'
    elif "description" in m:
        probe_id = str(m["probeId"])
        # probe_time = m["data"]["time"]
        probe_time = presentTime

        logger.info("Processing {probe} at {time}".format(
            probe=probe_id, time=probe_time))
        probe = sqldb.session.query(ProcessProbe).filter_by(
            probe_id=probe_id).first()

        if m.get("description") == "ipLog":
            newState = "PROBE ON"  # endState

            last_restart_state = last_restart_cause(probe, newState)
            restart_time = last_restart(probe_id, probe_time, newState)
            time_up = up_since(probe_id, probe_time)
            if last_restart_state is not None:
                probe.last_state = last_restart_state
            # update webpage
            emit_event = 'probe_on'
            card_event = 'active_state'
            emit_state = newState
            emit_id = remove_colon(probe_id)
            with probeTime_Lock:
                emit_probe_time = probeTimeDict[probe_id]
            emit_heartbeat = presentTime
            emit_restart_time = tz_aware_time(restart_time)
            emit_time_up = tz_aware_time(time_up)
            emit_last_state = probe.last_state

            card_params = [card_event, emit_state, emit_id, emit_probe_time]
            table_params = [emit_event, emit_state, emit_id,
                            emit_probe_time, emit_heartbeat, emit_time_up]
            detailed_params = [emit_event, emit_state,
                               emit_id, emit_probe_time, emit_time_up]

            card_emissions(*card_params)
            table_emissions(*table_params)
            detailed_emissions(*detailed_params)
            # update probe state dictionary
            with probeState_Lock:
                probeStateDict[probe_id] = newState
            # update database
            probe.state = newState

            probe.last_restart_time = restart_time
            probe.on_since = time_up
            sqldb.session.add(probe)
            sqldb.session.commit()
            sqldb.session.remove()

        elif m.get("description") == "shutdown":
            # probe_time = m["data"]["time"]
            probe_time = presentTime
            newState = "SHUTDOWN TRANSITION"
            probe.state = newState
            sqldb.session.add(probe)
            sqldb.session.commit()
            sqldb.session.remove()

            with probeState_Lock:
                probeStateDict[probe_id] = newState

        else:
            newState = "PROBE ON"

            last_restart_state = last_restart_cause(probe, newState)
            restart_time = last_restart(probe_id, probe_time, newState)
            time_up = up_since(probe_id, probe_time)
            if last_restart_state is not None:
                probe.last_state = last_restart_state

            emit_event = 'probe_on'
            card_event = 'active_state'
            emit_state = newState
            emit_id = remove_colon(probe_id)
            with probeTime_Lock:
                emit_probe_time = probeTimeDict[probe_id]
            emit_heartbeat = presentTime
            emit_restart_time = tz_aware_time(restart_time)
            emit_time_up = tz_aware_time(time_up)
            emit_last_state = probe.last_state

            card_params = [card_event, emit_state, emit_id, emit_probe_time]
            table_params = [emit_event, emit_state, emit_id,
                            emit_probe_time, emit_heartbeat, emit_time_up]
            detailed_params = [emit_event, emit_state,
                               emit_id, emit_probe_time, emit_time_up]

            card_emissions(*card_params)
            table_emissions(*table_params)
            detailed_emissions(*detailed_params)
            probe.state = newState

            probe.last_restart_time = restart_time
            probe.on_since = time_up
            sqldb.session.add(probe)
            sqldb.session.commit()
            sqldb.session.remove()

            with probeState_Lock:
                probeStateDict[probe_id] = newState
    elif "data" in m:
        probe_id = str(m["master"]["id"])
        # probe_time = m["master"]["time"]
        last_power = last_probe_power(m)
        probe_time = presentTime
        probe = sqldb.session.query(ProcessProbe).filter_by(
            probe_id=probe_id).first()
        logger.info("Processing {probe} at {time}".format(
            probe=probe_id, time=probe_time))
        newState = "PROBE ON"

        last_restart_state = last_restart_cause(probe, newState)
        restart_time = last_restart(probe_id, probe_time, newState)
        time_up = up_since(probe_id, probe_time)
        if last_restart_state is not None:
            probe.last_state = last_restart_state

        emit_event = 'probe_on_data'
        card_event = 'active_state'

        emit_state = newState
        emit_id = remove_colon(probe_id)
        with probeTime_Lock:
            emit_probe_time = probeTimeDict[probe_id]
        emit_heartbeat = presentTime
        emit_restart_time = tz_aware_time(restart_time)
        emit_time_up = tz_aware_time(time_up)
        emit_last_power = last_power
        emit_last_state = probe.last_state

        card_params = [card_event, emit_state, emit_id, emit_probe_time]
        table_params = [emit_event, emit_state, emit_id, emit_probe_time, emit_heartbeat,
                        emit_restart_time, emit_last_power, emit_time_up, emit_last_state]
        detailed_params = [emit_event, emit_state, emit_id,
                           emit_probe_time, emit_time_up, emit_last_power, emit_last_state]

        card_emissions(*card_params)
        table_data_emissions(*table_params)
        detailed_data_emissions(*detailed_params)

        probe.state = newState
        probe.last_power = last_power
        probe.last_restart_time = restart_time
        probe.on_since = time_up
        sqldb.session.add(probe)
        sqldb.session.commit()
        sqldb.session.remove()

        with probeState_Lock:
            probeStateDict[probe_id] = newState
    else:
        probe = sqldb.session.query(ProcessProbe).filter_by(
            probe_id=probe_id).first()
        newState = "ERROR: Wrong Message Format"
        time_up = probe.on_since

        emit_event = 'state_response'
        card_event = 'state_response'
        emit_state = newState
        emit_id = remove_colon(probe_id)
        with probeTime_Lock:
            emit_probe_time = probeTimeDict[probe_id]
        emit_heartbeat = presentTime
        emit_time_up = tz_aware_time(time_up)

        card_params = [card_event, emit_state, emit_id, emit_probe_time]
        table_params = [emit_event, emit_state, emit_id,
                        emit_probe_time, emit_heartbeat, emit_time_up]
        detailed_params = [emit_event, emit_state,
                           emit_id, emit_probe_time, emit_time_up]

        card_emissions(*card_params)
        table_emissions(*table_params)
        detailed_emissions(*detailed_params)

        probe.state = newState
        sqldb.session.add(probe)
        sqldb.session.commit()
        sqldb.session.remove()

        with probeState_Lock:
            probeStateDict[probe_id] = newState

    return newState, m


def shutdown_state_trans(m):
    presentTime = str(now("Africa/Lagos"))
    probe_id = str(m["probeId"])
    # probe_time = m["data"]["time"]
    probe_time = presentTime
    probe = sqldb.session.query(ProcessProbe).filter_by(
        probe_id=probe_id).first()
    if m["data"]["shutdown_complete"] == 'True':
        newState = "PROBE OFF"  # endState

        time_up = probe.on_since
        emit_event = 'probe_off'
        card_event = 'active_state'
        emit_state = newState
        emit_id = remove_colon(probe_id)
        # with probeTime_Lock:
        emit_probe_time = probe_time
        emit_heartbeat = presentTime
        emit_time_up = tz_aware_time(time_up)

        card_params = [card_event, emit_state, emit_id, emit_probe_time]
        table_params = [emit_event, emit_state, emit_id,
                        emit_probe_time, emit_heartbeat, emit_time_up]
        detailed_params = [emit_event, emit_state,
                           emit_id, emit_probe_time, emit_time_up]

        card_emissions(*card_params)
        table_emissions(*table_params)
        detailed_emissions(*detailed_params)

        probe.state = newState
        sqldb.session.add(probe)
        sqldb.session.commit()
        sqldb.session.remove()

        with probeState_Lock:
            probeStateDict[probe_id] = newState
    else:
        newState = "SHUTDOWN INITIALISED"

        time_up = probe.on_since
        emit_event = 'shutdown_initialised'
        card_event = 'active_state'

        emit_state = newState
        emit_id = remove_colon(probe_id)
        with probeTime_Lock:
            emit_probe_time = probeTimeDict[probe_id]
        emit_heartbeat = presentTime
        emit_time_up = tz_aware_time(time_up)

        card_params = [card_event, emit_state, emit_id, emit_probe_time]
        table_params = [emit_event, emit_state, emit_id,
                        emit_probe_time, emit_heartbeat, emit_time_up]
        detailed_params = [emit_event, emit_state,
                           emit_id, emit_probe_time, emit_time_up]

        card_emissions(*card_params)
        table_emissions(*table_params)
        detailed_emissions(*detailed_params)

        probe.state = newState
        sqldb.session.add(probe)
        sqldb.session.commit()
        sqldb.session.remove()

        with probeState_Lock:
            probeStateDict[probe_id] = newState

    return newState, probe_id


def tamper_check(m):
    probe_id = m['id']
    channel_states = []
    channel_info = []
    current_time = str(now("Africa/Lagos"))
    for report_info in m['report']:
        if report_info['sourceName'] != "":
            if report_info['state'] is True and report_info['changed'] is True:
                channel_state = 'SENSOR REPLUGGED'
                channel_num = report_info['sourceChannel']
                source_name = report_info['sourceName']

                channel_states.append(channel_state)
                channel_info.append((channel_num, source_name))
            elif report_info['state'] is False and report_info['changed'] is True:
                channel_state = 'SENSOR UNPLUGGED'
                channel_num = report_info['sourceChannel']
                source_name = report_info['sourceName']

                channel_states.append(channel_state)
                channel_info.append((channel_num, source_name))
            elif report_info['state'] is True and report_info['changed'] is False:
                channel_state = 'SENSOR UNTOUCHED'  # don't log
            else:
                channel_state = 'UNPLUGGED STATIC'  # don't log
        else:
            continue

    if len(channel_states) > 0:  # TODO: replace this with a function
        newState = 'PROBE TAMPERED'
        probe = sqldb.session.query(ProcessProbe).filter_by(
            probe_id=probe_id).first()
        tamper_params = zip(channel_states, channel_info)
        for state, info in tamper_params:
            channel_number, channel_source = info
            tamper_details = Tamper(tamper_time=current_time, owner=probe)
            tamper_details.channel_state = state
            tamper_details.channel_number = channel_number
            tamper_details.channel_source = channel_source
            sqldb.session.add(tamper_details)
        sqldb.session.commit()
        sqldb.session.remove()

    else:
        newState = 'PROBE UNTAMPERED'

    return newState, probe_id


def get_tamper_history(probe_id):
    logger.info("Resolving tamper history for {}".format(probe_id))
    probe = sqldb.session.query(ProcessProbe).filter_by(
        probe_id=probe_id).first()
    get_tamper_report = probe.tamper_report.all()
    tamper_history = []
    for report in get_tamper_report:
        tamper_instance = (report.channel_state, report.channel_number,
                           report.channel_source, report.tamper_time)
        tamper_history.append(tamper_instance)
    return list(reversed(tamper_history))


def message_processor(probeQ):
    probeQ_Lock.acquire()
    data = probeQ.get(True)
    probeQ_Lock.release()
    probeId, m = data

    if "probeId" in m:
        cmdctrl.run(m)

    elif "data" in m:
        cmdctrl.run(m)

    elif "report" in m:
        cmdctrl.run(m)

    else:
        try:
            logger.exception("message_processor Error")
            logger.exception("{}".format(m))
            pass
        except Exception:
            logger.exception("message_processor Error")
            print_exc(file=stdout)
            pass


def probe_check(m):
    presentTime = str(now("Africa/Lagos"))
    if "description" in m:
        probe_id = str(m["probeId"])
        probeQ.put((probe_id, m))
        # probe_time = m['data']['time']
        probe_time = presentTime
        probe = sqldb.session.query(ProcessProbe).filter_by(
            probe_id=probe_id).first()
        if probe is None:  # Then the probe is not in the database
            if not probeQ.full():
                logger.info(
                    "Creating new probe with probe ID {} ".format(probe_id))
                new_probe = ProcessProbe(probe_id=probe_id)
                new_probe.on_since = probe_time
                new_probe.last_probe_message = probe_time
                new_probe.last_heartbeat = presentTime
                with probeTime_Lock:
                    probeTimeDict[probe_id] = probe_time
                sqldb.session.add(new_probe)
                sqldb.session.commit()
                sqldb.session.remove()
        else:
            probe.last_probe_message = probe_time
            probe.last_heartbeat = presentTime
            with probeTime_Lock:
                probeTimeDict[probe_id] = probe_time
                probeTimeDict.get(probe_id) and probeTimeDict.update(
                    {probe_id: probe_time})
            sqldb.session.add(probe)
            sqldb.session.commit()
            sqldb.session.remove()
    elif "report" in m:
        probe_id = str(m["id"])
        probeQ.put((probe_id, m))

    elif "data" in m:
        probe_id = str(m["master"]["id"])
        probeQ.put((probe_id, m))
        # probe_time = m["master"]["time"]
        probe_time = presentTime
        probe = sqldb.session.query(ProcessProbe).filter_by(
            probe_id=probe_id).first()
        if probe is None:
            if not probeQ.full():
                logger.info(
                    "Creating new probe with probe ID {}".format(probe_id))
                new_probe = ProcessProbe(probe_id=probe_id)
                new_probe.on_since = probe_time
                new_probe.last_probe_message = probe_time
                new_probe.last_heartbeat = presentTime
                with probeTime_Lock:
                    probeTimeDict[probe_id] = probe_time
                sqldb.session.add(new_probe)
                sqldb.session.commit()
                sqldb.session.remove()
        else:
            probe.last_probe_message = probe_time
            probe.last_heartbeat = presentTime
            with probeTime_Lock:
                probeTimeDict[probe_id] = probe_time
                probeTimeDict.get(probe_id) and probeTimeDict.update(
                    {probe_id: probe_time})
            sqldb.session.add(probe)
            sqldb.session.commit()
            sqldb.session.remove()
    else:
        logger.exception("Invalid Message Format")
        logger.exception(m)


def message_consumer(messages):
    probe_check(messages)
    message_processor(probeQ)


def message_reader(kafkaQ):
    try:
        res = kafkaQ.get(True, 10)
    except Empty:
        if kafkaQ.qsize() < 1:
            logger.exception("kafkaQ is empty")
        else:
            logger.exception("kafkaQ not empty. get() timed out.")
        res = None
    if res is None:
        message, sender = None, None
        logger.warning("{} received no message from the consumers".format(
            current_process().name))
    else:
        message, sender = res
        logger.info("{} processing message from {} in {}".format(
            current_process().name, sender, current_thread().name))
    return res


def start_machine(kafkaQ):
    while True:
        sleep(.01)
        res = message_reader(kafkaQ)
        if res is None:
            continue
        else:
            messages, sender = res
            message_consumer(messages)


def transition_start():
    t = Thread(target=start_machine, args=(kafkaQ,))
    t.daemon = True
    t.name = 'transitions_thread'
    t.start()


def message_loader(message_gen):
    for message, name in message_gen:
        kafkaQ.put((message, name))


def de_serialize_message(message, name):
    """deserializes serialized messages using the decode_message method"""
    client = CachedSchemaRegistryClient(
        url='http://dashboard.grit.systems:8081')
    serializer = MessageSerializer(client)
    de_serialiazed_message = serializer.decode_message(message)
    return de_serialiazed_message, name


def yield_consumer_messages(consumer):
    for message in consumer:
        name = current_thread().name
        try:
            yield loads(message.value), name
        except ValueError:
            try:
                yield de_serialize_message(message.value, name)
            except Exception as e:
                logger.exception(e)
                logger.exception(message)
                continue
        # TODO: restart yield_consumer_messages after 5 seconds if there is an issue with connection to the brokers
        # i.e catch NoBrokersAvailable


def queue_consumer_messages(consumer):
    message_gen = yield_consumer_messages(consumer)
    message_loader(message_gen)


def consumer_threads(test_consumer, dashboard_consumer, kafkaQ):
# def consumer_threads(test_consumer, kafkaQ):
    threads = list()
    t1 = Thread(target=queue_consumer_messages, args=(test_consumer,))
    t2 = Thread(target=queue_consumer_messages, args=(dashboard_consumer,))
    t1.daemon = True
    t2.daemon = True
    t1.name = 'test_consumer_thread'
    t2.name = 'dashboard_consumer_thread'
    threads.append(t1)
    threads.append(t2)
    t1.start()
    t2.start()


def consumer_connect(kafkanode, topics):
    logger.info(
        "\n=================================\nstarting consumer\n=================================\n")
    try:
        consumer = KafkaConsumer(
            bootstrap_servers=kafkanode, group_id=group_id, auto_offset_reset='latest')
        consumer.subscribe(topics)
        consumer.poll()
        consumer.seek_to_end()
    except Exception:
        consumer = {'value': None}
        logger.exception("Error Connecting to {}".format(kafkanode))
    logger.info(
        "============================== waiting for message ====================================")
    return consumer


def start_consumer():
    test_consumer = consumer_connect(test_kafkanode, topics)
    dashboard_consumer = consumer_connect(dashboard_kafkanode, topics)
    consumer_threads(test_consumer, dashboard_consumer, kafkaQ)
    # consumer_threads(test_consumer, kafkaQ)


def probe_status():
    probe = sqldb.session.query(ProcessProbe).order_by(
        ProcessProbe.probe_id).all()
    probeLength = len(probe)
    if probeLength > 0:
        logger.info("There are {} probes in the database.".format(probeLength))
    else:
        logger.info("There are 0 probes in the database :(")


def create_state_dict(probeStateId, probeStateList):
    sqldb_probes = sqldb.session.query(ProcessProbe).all()
    for sqldb_probe in sqldb_probes:
        probeStateList.append(str(sqldb_probe.state))
        probeStateId.append(str(sqldb_probe.probe_id))

    return dict(zip(probeStateId, probeStateList))


def probe_loader(probeIdList, probeTimeList):
    sqldb_probes = sqldb.session.query(ProcessProbe).all()
    for sqldb_probe in sqldb_probes:
        probeIdList.append(sqldb_probe.probe_id.encode('utf-8'))
        probeTimeList.append(str(sqldb_probe.last_probe_message))

    logger.info("{} probes loaded from database".format(len(probeIdList)))
    return dict(zip(probeIdList, probeTimeList))


def load_to_sql(probeList):
    for item in probeList:
        try:
            probe = sqldb.session.query(
                ProcessProbe).filter_by(probe_id=item).first()
            # probe = ProcessProbe.query.filter_by(probe_id=item).first()
            new_probe = ProcessProbe(probe_id=item)
            if probe is None:
                logger.info(
                    "Loading {probe} to postgres database".format(probe=item))
                new_probe = ProcessProbe(probe_id=item)
                try:
                    sqldb.session.add(new_probe)
                    sqldb.session.commit()
                    sqldb.session.remove()
                except Exception as errn:
                    logger.exception("Session Error when committing probe {function}: {errn}".format(
                        function=load_to_sql, errn=errn))
                finally:
                    sqldb.session.rollback()
            else:
                pass
        except Exception as errn:
            logger.exception("ERROR CREATING PROBE: {errn}".format(errn=errn))


def load_from_mongo(probeList):
    for p in probes.find({}):
        probeList.append(p.get('probeName'))
    return probeList


def initiate_states():
    cmdctrl.addState("START", trans_controller)
    cmdctrl.addState("SHUTDOWN TRANSITION", shutdown_state_trans)
    cmdctrl.addState("TAMPER_CHECK", tamper_check)
    # cmdctrl.addState("TAMPER_CHECK", tamper_check)
    cmdctrl.addState("PROBE TAMPERED", None, endState=1)
    cmdctrl.addState("PROBE UNTAMPERED", None, endState=1)
    # cmdctrl.addState("SENSOR UNTOUCHED", None, endState=1)
    # cmdctrl.addState("UNPLUGGED STATIC", None, endState=1)

    cmdctrl.addState("OFFLINE", None, endState=1)
    cmdctrl.addState("PROBE ON", None, endState=1)
    cmdctrl.addState("PROBE OFF", None, endState=1)
    cmdctrl.addState("BATTERY FAILURE", None, endState=1)
    cmdctrl.addState("SHUTDOWN INITIALISED", None, endState=1)
    cmdctrl.addState("ERROR: Wrong Message Format", None, endState=1)
    cmdctrl.setStartState("START")
    logger.info("STATES INITIALISED")


def content_page_creator(probe_keyset, content_health_params, config_keyset):
    column_num = 1
    doc = document(title='Command and Control')
    with doc:
        with div():
            raw("\n Toggle Column:")
            for param in content_health_params:
                a(str(param) + "    |   ", cls="toggle-vis", data_column=column_num)
                column_num += 1

            for config_keys in config_keyset:
                if config_keys in content_ignored_params:
                    pass
                else:
                    a(str(config_keys) + "    |   ",
                      cls="toggle-vis", data_column=column_num)
                    column_num += 1

            for probe_keys in probe_keyset:
                if probe_keys in content_ignored_params:
                    pass
                else:
                    a(str(probe_keys) + "    |   ",
                      cls="toggle-vis", data_column=column_num)
                    column_num += 1

        with table(id="probelist", border="1", cls="display", cellspacing="0", width="100%"):
            head = thead().add(tr())
            for param in content_health_params:
                head += th(str(param))

            for config_keys in config_keyset:
                if config_keys in content_ignored_params:
                    pass
                else:
                    head += th(str(config_keys))

            for probe_keys in probe_keyset:
                if probe_keys in content_ignored_params:
                    pass
                else:
                    head += th(str(probe_keys))

            with tbody():
                raw("\n\t    {% for probe in probe_configs %}")
                raw("\n\t    {% set probe_id = probe.probeName %}")
                raw("\n\t    {% set test = sqldb.session.query(ProcessProbe).filter_by(probe_id=probe_id).first() %}")
                with tr():
                    for param, val in zip(content_health_params, content_sqldb_params):
                        if param == "probe_state":
                            td("{{ test." + val + "}}", id=str(param) +
                               "{{ probe.probeName | remove_colon }}",
                               cls="{{ test." + val + " | refactor_this}}")
                        else:
                            td("{{ test." + val + "}}", id=str(param) +
                               "{{ probe.probeName | remove_colon }}")

                    for config_keys in config_keyset:
                        if config_keys in content_ignored_params:
                            pass
                        else:
                            td("{{ get_config_name(probe) }}", cls="c1")

                    for probe_keys in probe_keyset:
                        if probe_keys in content_ignored_params:
                            pass
                        elif probe_keys == "probeName":
                            td("{{ probe." + probe_keys + "}}", cls="c1")
                        else:
                            td("{{ probe." + probe_keys + "}}", cls="c1")
                raw("\n    {% endfor %}")

    with open('templates/content.html', 'w') as f:
        f.write(doc.render())


def build_table_content():
    results = []
    for probe in probes.find():
        probe_id = probe.get('probeName')
        res = sqldb.session.query(ProcessProbe).filter_by(
            probe_id=probe_id).first()
        try:
            result = dict(state=str(res.state),
                          last_probe_message=str(res.last_probe_message),
                          configName=str(device_config_name(probe)),
                          probeName=str(probe_id))
            results.append(result)
        except AttributeError as e:
            logger.exception(e)
            continue
    return results


def device_config_name(probe):
    try:
        configId = probe['Channels'][0].get('ConfigID_FK')
        get_config = configurations.find_one({'_id': configId})
        if isinstance(get_config, dict):
            config_name = get_config['configurationName']
        else:
            config_name = 'Not_Defined'
            configId = probe['Channels'][1].get('ConfigID_FK')
            get_config = configurations.find_one({'_id': configId})
            if isinstance(get_config, dict):
                config_name = get_config['configurationName']
            else:
                config_name = 'Not_Defined'
    except TypeError:
        config_name = 'Not_Defined'
        pass
    except IndexError:
        config_name = 'Not_Defined'
        pass
    return config_name


def cmdctrl_cards():
    doc = document(title='Command and Control')
    with doc:
        with div(id='uninitialized_probes'):
            with div(cls='container-fluid container_color'):
                small('Uninitialized Probes', cls='h6 container_text')
                with div(cls='row'):
                    raw("\n\t   {% for res in results %}")
                    with div(cls='card card_small m-1 uninitialized_probes', id='probe{{ res.probeName | remove_colon }}'):
                        a(href="{{'/details/' +res.probeName}}")
                        with div(cls='card-block'):
                            div('{{ res.configName}}', cls='conf_name',
                                id='configurationName{{ res.probeName | remove_colon }}')
                            div('{{ res.last_probe_message | humanize_time }}', cls='probe_time',
                                id='last_probe_message{{ res.probeName | remove_colon }}')
                            div('{{ res.state }}', cls='probe_state',
                                id="probe_state{{ res.probeName | remove_colon }}")
                            div('{{ res.probeName }}', cls='text-truncate probe_id',
                                id='probeName{{ res.probeName | remove_colon }}')
                    raw("\n    {% endfor %}")
    with open('templates/cmdctrl_card.html', 'w') as f:
        f.write(doc.render())


def probe_details_view():
    doc = document(title='{{registered_user}}')
    with doc:
        with div(cls='container'):
            with table(cls='table'):
                with tbody():
                    with tr():
                        th("{{device_owner}}")
                        td("{{registered_user}}",
                           id='{{device_owner | refactor_this}}')
                    raw("\n\t{% for param, gres in probe_details_params %}")
                    with tr():
                        th("{{param}}")
                        td("{{gres}}", id='{{param | refactor_this}}' +
                           '{{probe_id | remove_colon}}')
                    raw("\n\t{% endfor %}")
                    raw("{% for detail, mongo in probe_mongo_params %}")
                    with tr():
                        th("{{detail}}")
                        td("{{mongo_probe[mongo]}}", id='{{detail | refactor_this}}' +
                           '{{probe_id | remove_colon}}')
                    raw("\n\t{% endfor %}")
    with open('templates/probe_details.html', 'w') as f:
        f.write(doc.render())


def tamper_history_page():
    doc = document(title='')
    with doc:
        with div(cls='container'):
            with table(cls='table', id='tamper-history'):
                with thead().add(tr()):
                    raw("\n\t    {% for header in tamper_headers %}")
                    th("{{ header }}")
                    raw("\n    {% endfor %}")
                with tbody():
                    raw("\n\t    {% for history in tamper_history %}")
                    raw("\n\t    {% set channel_state, channel_number, channel_source, tamper_time = history %}")
                    with tr():
                        td("{{ channel_state }}")
                        td("{{ channel_number }}")
                        td("{{ channel_source }}")
                        td("{{ tamper_time }}")
                    raw("\n    {% endfor %}")

    with open('templates/tamper_content.html', 'w') as f:
        f.write(doc.render())


# route for handling the login page logic
@app.route('/login', methods=['GET', 'POST'])
def login():
    if session.get('logged_in'):
        return redirect(url_for('index'))
    else:
        pass
    usermail = request.form['username']
    userpass = request.form['password']
    error = None
    if request.method == 'POST':
        result = users.find_one({"email": usermail})
        if result and result.get('roles')[0] == 'admin':
            passwordHash = result.get('password')
            if passwordHash != "":
                encoded_hash = passwordHash.encode("utf-8")
                if bcrypt.hashpw(userpass.encode("utf-8"), encoded_hash) == encoded_hash:
                    session['logged_in'] = True
        elif result is None:
            result = sqldb.session.query(
                User).filter_by(email=usermail).first()
            if result:
                if result.check_password(userpass):
                    session['logged_in'] = True
        else:
            logger.warn("Invalid login from {usermail} and {userpass}".format(
                usermail=usermail, userpass=userpass))
            error = 'Invalid Credentials. Please try again.'
        return index()
    return render_template('login.html', error=error)


@app.route("/cmdctrl_table")
def cmdctrl_table():
    if not session.get('logged_in'):
        return render_template('login.html')
    elif session['logged_in'] is False:
        # return login()
        return redirect(url_for('login'))
    else:
        last_probe_states()
        return render_template('cmdctrl_table.html', async_mode=socketio.async_mode, sqldb=sqldb, ProcessProbe=ProcessProbe, probe_configs=probes.find())


@app.route("/")
def index():
    if not session.get('logged_in'):
        return render_template('login.html')
    elif session['logged_in'] is False:
        # return login()
        return redirect(url_for('login'))
    else:
        last_probe_states()
        return render_template('index.html', results=build_table_content(), ProcessProbe=ProcessProbe, probe_configs=probes.find())


@app.route("/details/<string:probe_id>")
def probe_details(probe_id):
    validity = check_probe_validity(probe_id)
    if validity == 'Valid Probe':
        mongo_probe = probes.find_one({'probeName': probe_id})
        registered_user = device_config_name(mongo_probe).upper()
        device_owner = "DEVICE OWNER"
        mongo_params = ["ScalingFactorCurrent",
                        "ScalingFactorVoltage", "NumberOfChannels"]
        postgres_params = get_postgres_params(probe_id)
        return render_template('device_info.html', probe_details_params=postgres_params, probe_mongo_params=zip(probe_details_mongo_params, mongo_params), mongo_probe=mongo_probe, device_owner=device_owner, registered_user=registered_user, probe_id=probe_id)
    else:
        return render_template('invalid_probe.html')


@app.route("/tamper_history/<string:probe_id>")
def tampered_probes(probe_id):
    validity = check_probe_validity(probe_id)
    if validity == 'Valid Probe':
        tamper_history = get_tamper_history(probe_id)
        if tamper_history is None:
            return render_template('untampered_probe.html')
        else:
            return render_template('tamper_history.html', tamper_headers=tamper_headers, tamper_history=tamper_history)
    else:
        return render_template('invalid_probe.html')


@app.route("/details")
@app.route("/tamper_history")  #TODO: Create function for all tampered probes
def no_probeId():
    return render_template('no_probeId.html')


# @app.route("/tamper_history")
# def view_all_tampered_probes():
#     pass


@app.route("/logout")
def logout():
    session['logged_in'] = False
    return index()


@app.template_filter('remove_colon')
def remove_colon(s):
    return re.sub(':', '', s)


@app.template_filter('humanize_time')
def humanize_time(t):
    try:
        present_time = now('Africa/Lagos')
        arrow_time = get(str(t)).replace(tzinfo='Africa/Lagos')
        humanized_time = arrow_time.humanize(present_time)
        return humanized_time
    except RuntimeError:
        pass


@app.template_filter('refactor_this')
def refactor_this(s):
    return s.lower().replace(" ", "_")


@app.context_processor
def configName():
    def get_config_name(probe):
        try:
            configId = probe['Channels'][0].get('ConfigID_FK')
            get_config = configurations.find_one({'_id': configId})
            if isinstance(get_config, dict):
                config_name = get_config['configurationName']
            else:
                config_name = 'Not_Defined'
            return config_name
        except Exception as e:
            logger.exception(e)
            config_name = 'Not Found'
            return config_name
    return dict(get_config_name=get_config_name)


def check_probe_validity(probe_id):
    probe = sqldb.session.query(ProcessProbe).filter_by(
        probe_id=probe_id).first()
    if probe is not None:
        return 'Valid Probe'


def get_postgres_params(probe_id):
    probe = sqldb.session.query(ProcessProbe).filter_by(probe_id=probe_id).first()
    new_probe_details_postgres = []
    new_probe_details_postgres.append(probe.state)
    new_probe_details_postgres.append(probe.probe_id)
    new_probe_details_postgres.append(probe.last_probe_message)
    new_probe_details_postgres.append(probe.on_since)
    new_probe_details_postgres.append(probe.last_state)
    new_probe_details_postgres.append(probe.last_power)
    new_probe_details_postgres.append(probe.messages_per_hour)
    new_probe_details_postgres.append(probe.messages_per_day)
    postgres_params = zip(probe_details_postgres_params, new_probe_details_postgres)
    # new_probe_details_postgres = ['probe.state', 'probe.probe_id', 'probe.last_probe_message', 'probe.on_since',
    #                               'probe.last_state', 'probe.last_power', 'probe.messages_per_hour', 'probe.messages_per_day']
    # return [eval(i) for i in new_probe_details_postgres]
    return postgres_params


def last_probe_power(m):
    """The sum of the last power measured by each probe."""
    try:
        last_power_from_sources = m['data'][0]['powerSinceLast']
        total_power = 0
        for power in last_power_from_sources:
            total_power += power['powerSinceLast']
    except KeyError:
        total_power = 0
    total_power = float('{:.{precision}f}'.format(total_power, precision=2))
    return total_power


def last_probe_states():
    probes = sqldb.session.query(ProcessProbe).all()
    for probe in probes:
        if probe.last_probe_message is None or probe.last_heartbeat is None:
            probe.state = "UNINITIALIZED PROBE"
            pass
        sqldb.session.add(probe)
    sqldb.session.commit()
    sqldb.session.remove()


def last_restart(probe_id, probe_time, newState):
    off_states_set = {"PROBE OFF", "SHUTDOWN INITIALISED"}
    probe = sqldb.session.query(
        ProcessProbe).filter_by(probe_id=probe_id).all()
    probe = probe[0]
    restart_time = probe.last_restart_time
    if newState in off_states_set:
        restart_time = probe_time
    return restart_time


def last_restart_cause(probe, newState):
    """
    Note that the newState is always going to be PROBE ON due to the place it is used in the code
    """
    last_state = probe.state
    if last_state == newState:
        last_restart_state = probe.last_state
    else:
        last_restart_state = last_state
    return last_restart_state


def text_notifier(phone_numbers, probeId, newState, emit_probe_time):
    account_sid = app.config['ACCOUNT_SID']
    auth_token = app.config['AUTH_TOKEN']
    client = Client(account_sid, auth_token)

    for number in phone_numbers:
        message = client.messages.create(
            to=number,
            from_="+15595495622",
            body="{probeId} {newState} at {time}".format(probeId=probeId,
                                                         newState=newState, time=emit_probe_time))

    logger.info(message.sid)


@socketio.on("connected", namespace='/kstream')
def connection_notifier(connected):
    """Maybe include the queue check here"""
    print"[x] Received\t: ", connected
    server_message = 'Initiating Command and Control'
    emit("server_channel", server_message)


@socketio.on("card_connected", namespace='/card_stream')
def card_connection_notifier(connected):
    print"[x] Received Card Message\t: {}\n".format(connected)
    card_message = 'Message Transmission Commenced'
    emit("card_channel", card_message)


@socketio.on("info_connected", namespace='/info_stream')
def info_connection_notifier(connected):
    print"[x] Received Info Message\t: {}\n".format(connected)
    info_message = 'Beginning Message Streaming'
    emit("info_channel", info_message)


def card_emissions(card_event, emit_state, emit_id, emit_probe_time):
    socketio.emit(card_event, {'state': emit_state, 'id': emit_id, 'probe_time': humanize_time(
        emit_probe_time)}, namespace='/card_stream')


def table_emissions(emit_event, emit_state, emit_id, emit_probe_time, emit_heartbeat, emit_time_up):
    socketio.emit(emit_event, {'state': emit_state, 'id': emit_id, 'probe_time': emit_probe_time,
                               'socket_time': emit_heartbeat, 'time_up': str(emit_time_up)}, namespace='/kstream')


def table_data_emissions(emit_event, emit_state, emit_id, emit_probe_time, emit_heartbeat, emit_restart_time, emit_last_power, emit_time_up, emit_last_state):
    socketio.emit(emit_event, {'state': emit_state, 'id': emit_id,
                               'probe_time': emit_probe_time,
                               'socket_time': emit_heartbeat,
                               'restart_time': str(emit_restart_time),
                               'last_power': str(emit_last_power),
                               'time_up': str(emit_time_up), 'last_state': emit_last_state}, namespace='/kstream')


def detailed_emissions(emit_event, emit_state, emit_id, emit_probe_time, emit_time_up):
    socketio.emit(emit_event, {'state': emit_state, 'id': emit_id, 'probe_time': emit_probe_time, 'time_up': str(
        emit_time_up)}, namespace='/info_stream')


def detailed_data_emissions(emit_event, emit_state, emit_id, emit_probe_time, emit_time_up, emit_last_power, emit_last_state):
    socketio.emit(emit_event, {'state': emit_state, 'id': emit_id,
                               'probe_time': emit_probe_time,
                               'last_power': str(emit_last_power),
                               'time_up': str(emit_time_up), 'last_state': emit_last_state}, namespace='/info_stream')


def restarts_per_day():
    # TODO: number of state changes to probe on
    # TODO: put this inside places where the probe state is set.
    pass


def message_loss(probe_id):
    """This is the difference between the expected number of messages and total number of messages over a time period"""
    probe = sqldb.session.query(ProcessProbe).filter_by(
        probe_id=probe_id).first()
    # Input: Expected message frequency
    hourly_expectation = probe.expected_messages_per_hour
    daily_expectation = probe.expected_messages_per_day
    # Input: Real message frequency
    hourly_message_count = probe.messages_per_hour
    daily_message_count = probe.messages_per_day
    # TODO: Calculate message loss in percent
    hourly_message_loss = hourly_expectation - hourly_message_count
    daily_message_loss = daily_expectation - daily_message_count
    probe.hourly_message_loss = hourly_message_loss
    probe.daily_message_loss = daily_message_loss
    sqldb.session.commit()
    sqldb.session.remove()
    # TODO: Return message_loss
    return hourly_message_loss, daily_message_loss


def message_loss_rate(hourly_message_loss, daily_message_loss):
    # TODO: get number of hours from midnight till now
    # TODO: hourly_message_loss in an hour
    pass


def power_transitions():
    """This is the number of times messages were got from the different power sources per day or per hour"""
    pass


def query_probes(messageQ):
    try:
        probes = sqldb.session.query(ProcessProbe).all()
        for probe in probes:
            # for probe in ProcessProbe.query.all():
            if probe.state != "UNINITIALIZED PROBE":
                messageQ.put((probe.probe_id, probe.messages_per_hour,))
        for i in range(2):  # 2 sentinels put for the two threads consuming from messageQ
            messageQ.put('Done')
    except Exception as err:
        print err
        sqldb.session.rollback()
        raise
    sqldb.session.remove()


def emits_per_hour(probe_id, message_count):
    emit_event = 'msgs_per_hour'
    emit_id = remove_colon(probe_id)
    emit_msg_num = message_count

    socketio.emit(emit_event, {
                  'id': emit_id, 'msg_num': emit_msg_num},
                  namespace='/kstream')


def es_endpoint_url_builder(messageQ, resultQ):
    """
    sentinel count included because there are two producers to resultQ
    (each putting a sentinel vlue into the queue) while there is only one consumer
    """
    start_time = now("Africa/Lagos").shift(hours=-1)
    stop_time = now("Africa/Lagos")
    start_time = reformat_time(start_time)
    stop_time = reformat_time(stop_time)

    sentinel_count = 0
    while True:
        item = messageQ.get()
        if item is 'Done':
            if sentinel_count < 1:
                sentinel_count += 1
                if sentinel_count > 1:
                    print 'putting sentinel'
                    # signifies that there is no more message from the url
                    # query
                    resultQ.put('Done')
                    break
        else:
            probe_id, messages_per_hour = messageQ.get()
            try:
                url = "https://dashboard.grit.systems/cmdctrl/{0}/{1}/{2}".format(
                    probe_id, start_time, stop_time)
                res = rget(url, timeout=1)
                print "from try block"
                message_count = int(res.text)
            except Exception as err:
                # log err
                message_count = messages_per_hour
                print "from except block"
            resultQ.put((probe_id, message_count,))
            emits_per_hour(probe_id, message_count)


def db_update(resultQ):
    while True:
        item = resultQ.get()
        if item is 'Done':
            resultQ.task_done()
            break
        else:
            probe_id, message_count = item
            print 'Updating {} and {}'.format(probe_id, message_count)
            probe_id = sqldb.session.query(
                ProcessProbe).filter_by(probe_id=probe_id).first()
            probe_id.messages_per_hour = message_count
            sqldb.session.commit()
            sqldb.session.remove()
            resultQ.task_done()
    print '-----------updated database-------------'


def hourly_messenger():
    probe_query_thread = Thread(target=query_probes, args=(messageQ,))
    endpoint_request_thread1 = Thread(
        target=es_endpoint_url_builder, args=(messageQ, resultQ,))
    endpoint_request_thread2 = Thread(
        target=es_endpoint_url_builder, args=(messageQ, resultQ,))
    hourly_db_update_thread = Thread(target=db_update, args=(resultQ,))

    working_threads = [probe_query_thread, endpoint_request_thread1,
                       endpoint_request_thread2, hourly_db_update_thread]

    for thread in working_threads:
        thread.daemon = True
        thread.start()


def hourly_scheduler():
    # schedule.every(2).minutes.do(hourly_messenger)
    schedule.every().hour.do(hourly_messenger)
    while True:
        time.sleep(.01)
        schedule.run_pending()


def daily_scheduler():
    # schedule.every(5).minutes.do(daily_messenger)
    schedule.every().day.do(daily_messenger)
    while True:
        time.sleep(.01)
        schedule.run_pending()


def start_hourly_scheduler():
    h = threading.Thread(target=hourly_scheduler)
    h.start()


def emits_per_day(probe_id, message_count):
    emit_event = 'msgs_per_day'
    emit_id = remove_colon(probe_id)
    emit_msg_num = message_count

    socketio.emit(emit_event, {
                  'id': emit_id, 'msg_num': emit_msg_num},
                  namespace='/kstream')


def daily_db_update(resultQ):
    # TODO: update each probe_id and message count
    while True:
        item = resultQ.get()
        if item is 'Done':
            resultQ.task_done()
            break
        else:
            probe_id, message_count = item
            print 'Updating {} and {}'.format(probe_id, message_count)
            # sqldb_probes = ProcessProbe.query.all()
            probe_id = sqldb.session.query(
                ProcessProbe).filter_by(probe_id=probe_id).first()
            probe_id.messages_per_day = message_count
            sqldb.session.commit()
            sqldb.session.remove()
            resultQ.task_done()
    print '-----------updated database-------------'


def daily_endpoint_url_builder(daily_messageQ, daily_resultQ):
    """
    sentinel count included because there are two producers to daily_resultQ
    (each putting a sentinel vlue into the queue) while there is only one consumer
    """
    start_time = now("Africa/Lagos").shift(days=-1)
    stop_time = now("Africa/Lagos")
    start_time = reformat_time(start_time)
    stop_time = reformat_time(stop_time)

    sentinel_count = 0
    while True:
        item = daily_messageQ.get()
        if item is 'Done':
            if sentinel_count < 1:
                sentinel_count += 1
                if sentinel_count > 1:
                    print 'putting sentinel'
                    # signifies that there is no more message from the url
                    # query
                    resultQ.put('Done')
                    break
        else:
            probe_id, messages_per_hour = daily_messageQ.get()
            try:
                url = "https://dashboard.grit.systems/cmdctrl/{0}/{1}/{2}".format(
                    probe_id, start_time, stop_time)
                res = rget(url, timeout=1)
                print "from try block"
                message_count = int(res.text)
            except Exception as err:
                # log err
                message_count = messages_per_hour
                print "from except block"
            daily_resultQ.put((probe_id, message_count,))
            emits_per_day(probe_id, message_count)


def daily_messenger():
    # global daily_call

    probe_query_thread = Thread(target=query_probes, args=(daily_messageQ,))
    daily_request_thread1 = Thread(
        target=daily_endpoint_url_builder, args=(daily_messageQ, daily_resultQ,))
    daily_request_thread2 = Thread(
        target=daily_endpoint_url_builder, args=(daily_messageQ, daily_resultQ,))
    daily_db_update_thread = Thread(
        target=daily_db_update, args=(daily_resultQ,))

    working_threads = [probe_query_thread, daily_request_thread1,
                       daily_request_thread2, daily_db_update_thread]

    for thread in working_threads:
        thread.daemon = True
        thread.start()


def start_daily_scheduler():
    d = threading.Thread(target=daily_scheduler)
    d.start()


cmdctrl = CtrlStateMachine()
initiate_states()

probeQ = Queue(maxsize=0)
socketQ = Queue(maxsize=0)
kafkaQ = Queue(maxsize=0)
messageQ = Queue(maxsize=0)
resultQ = Queue(maxsize=0)
daily_messageQ = Queue(maxsize=0)
daily_resultQ = Queue(maxsize=0)

sqldb.create_all()
# sqldb.session.commit()
create_cmdctrl_user()

# template creators
cmdctrl_cards()
probe_details_view()
content_page_creator(probe_keyset, content_health_params, config_keyset)
tamper_history_page()

# probeTimeDict params
probeIdList = []
probeTimeList = []
probeStateList = []
probeStateId = []
shutdownList = []
probeList = []


last_probe_id = []
last_probe_state = []


# Database Initializers
load_from_mongo(probeList)
load_to_sql(probeList)


probeTimeDict = probe_loader(probeIdList, probeTimeList)
probeStateDict = create_state_dict(probeStateId, probeStateList)

# locks
probeTime_Lock = Lock()
probeState_Lock = Lock()
probeQ_Lock = Lock()
socketQ_Lock = Lock()

next_call = time.time()
hourly_call = time.time()
daily_call = time.time()

phone_numbers = ["+2348056041614", ]

probe_status()
try:
    start_consumer()
except Exception:
    logger.exception('================== {} KAFKA Error ==============='.format(
        start_consumer.__name__))
transition_start()
call_offline_checker()
start_hourly_scheduler()
start_daily_scheduler()


# TODO: Create link for viewing tamper history of individual probes
# TODO: display probe status changes in the last day and hour.
# TODO: last on duration
if __name__ == "__main__":
    socketio.run(app)
