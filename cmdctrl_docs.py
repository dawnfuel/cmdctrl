"""
Command and Control
Command and control provides a web interface for monitoring the states of field devices.
It will monitor all probes, monitors meters and other connected devices in the field and give an overview over device health,
perfomance and notify operators of abnormalities in the function of a probe at any time


To run:
1. Run the dependencies (start_cmdctrl.sh)
2. Run the state machine (cmdTransitions.py)

Code Parts
The code can be divided into x parts or blocks.
They are:

1. The State Machine class - CtrlStateMachine:
This class is found in cmdMachine.py.
It consists of 3 data structures and 3 methods.
The data structures are:
    a). A dictionary (stateFunctions) holding the states of the device as well as the functions handling those states.
    b). A list of possible end states (endStates)
    c). The start state (startState)
The methods are:
    a). addState - This adds the possible transition states that the probes (devices) could
            be in along with the functions handling those states (stateFunction). It
            also adds the possible end states to the list of end states.
    b). setStartState - This sets the start state of the state machine.
    c). run - This launches the state machine. It runs the function handling the
            start state and returns the new state as well as the message processed.
            if the new state is an end state, it breaks. If the state is a transitionstate,
            it runs the state function handling the transition state.
            This process is repeated till an end state is reached.

2. The Database
For this project, two databases are used. The MongoDB database is read from and
the postgres database is written to. This is to different processes writing to
the main MongoDB database. The models are declared in class ProcessProbe.
Flask_sqlalchemy is used for SQLAlchemy support.

3. The Kafka Consumer
This is the getMessages function generator. It consumes messages from the kafka producer.

4. The State Functions
When they are called by cmdctrl.run(data), transController, timeChecker and
shutdownStateTrans look at the messages from kafka consumer, determine the
states of the probe and emit the state to the web app via SocketIO.

5. Probe Checker
This function - probeCheck -
    adds each message to a queue for processing and looks at the probe ID of every message and
checks the database if a message has been received from that ID before.
If no message has been received from that ID before, it adds the device to the database.

6. Message Processor
The function messageProcessor takes a message queue from the probeCheck function
and processes valid messages using the state functions -- cmdctrl.run(m).

7. Message Consumer
This function (messageConsumer) consumes the kafka messages from the kafka
consumer function generator. it then calls the probe checker and message
processor (explained above) functions.

8. Offline Checker
offlineChecker loops through (probeTimeDict) the dictionary of probe IDs and the last time a
message was received from each probe ID. It sets the probe state to offline if the
message was received more than 15 seconds ago.

9. The Offline Checker Caller
This starts a timer thread and calls offlineChecker every 5 seconds. It is
started in a timer thread as soon as the program (cmdTransitions) starts running.



Server
Gevent server is used instead of WSGI server which comes with flask as WSGI is
a synchronous server while gevent is an asynchonous one.
"""

"""
Shutdown Complete checker Sets the probe state to battery failure if the status
has been SHUTDOWN INITIALISED for more than 60 seconds.

This is done by calling the function 70 seconds after the SHUTDOWN INITIALISED
state has been set. To prevent the program from stopping code execution during
this time, a separate thread is created to handle this process. This thread is
in the shutdown caller function (shutdownCaller).
"""
