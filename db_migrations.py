#!/usr/bin/env python
from flask import Flask
from flask_environments import Environments
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
import os
from pprint import pprint
app = Flask(__name__)
# app.secret_key = os.urandom(12)

env = Environments(app)
env.from_object('config')
print env

print(app.config)

app.config['SQLALCHEMY_DATABASE_URI'] = app.config['POSTGRES_DATABASE']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

sqldb = SQLAlchemy(app)
migrate = Migrate(app, sqldb)


class ProcessProbe(sqldb.Model):
    id = sqldb.Column(sqldb.Integer, primary_key=True,
                      autoincrement=True, unique=True)
    probe_id = sqldb.Column(sqldb.String(50), unique=True)
    last_probe_message = sqldb.Column(sqldb.DateTime())
    last_heartbeat = sqldb.Column(sqldb.DateTime())
    # probe_changes = sqldb.Column(sqldb.Integer())
    on_since = sqldb.Column(sqldb.DateTime())
    # create new column for message_interval. will be for
    message_interval = sqldb.Column(sqldb.Interval(second_precision=6))
    messages_per_hour = sqldb.Column(sqldb.Integer(), default=0)
    messages_per_day = sqldb.Column(sqldb.Integer(), default=0)
    state_change_count = sqldb.Column(sqldb.Integer(), default=0)
    last_power = sqldb.Column(sqldb.Float())
    last_state = sqldb.Column(sqldb.String(50))

    last_restart_time = sqldb.Column(sqldb.DateTime())
    timeout = sqldb.Column(sqldb.Integer(), default=5)
    state = sqldb.Column(sqldb.String(50))
    expected_messages_per_hour = sqldb.Column(sqldb.Integer(), default=0)
    expected_messages_per_day = sqldb.Column(sqldb.Integer(), default=0)
    avg_message_frequency = sqldb.Column(sqldb.Integer())
    avg_heartbeat_frequency = sqldb.Column(sqldb.Integer())

    def __init__(self, probe_id):
        self.probe_id = probe_id
