#!/bin/bash
source cmd_env/bin/activate
export FLASK_DEBUG=0
export FLASK_APP=cmdTransitions.py
#python -m flask run
gunicorn -k geventwebsocket.gunicorn.workers.GeventWebSocketWorker -w 1 --timeout 120 cmdTransitions:app
