from flask import Flask
from flask_environments import Environments
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Api


app = Flask(__name__)
env = Environments(app)
env.from_object('config')

sqldb = SQLAlchemy(app)
api = Api(app)

from .models import Probes

sqldb.Model.metadata.reflect(sqldb.engine)
