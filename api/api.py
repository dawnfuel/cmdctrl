#!/usr/bin/env python


# from cmd_api import create_app, sqldb, api
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Api, Resource
from flask_environments import Environments
from sqlalchemy import or_
from arrow import get
from functools import wraps
# from config import config, Config

app = Flask(__name__)
env = Environments(app)
env.from_object('config')


sqldb = SQLAlchemy(app)
auth = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API-KEY'
    }
}

api = Api(app, authorizations=auth)

sqldb.Model.metadata.reflect(sqldb.engine)


class Probes(sqldb.Model):
    __table__ = sqldb.Model.metadata.tables['process_probe']


def reformat_time(time):
    new_time_format = get(time).format('YYYY-MM-DDTHH:mm:ss.SSSZZ')
    return new_time_format


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        token = None

        if 'X-API-KEY' in request.headers:
            token = request.headers['X-API-KEY']

        if not token:
            return {'message': 'Token is missing.'}, 401

        else:
            # Check for token validity in database
            pass

        print('TOKEN: {}'.format(token))
        return f(*args, **kwargs)

    return decorated


def offline_probes(ProcessProbe):
    dump = []
    offline_probes = sqldb.session.query(ProcessProbe).filter(or_(ProcessProbe.state == 'OFFLINE', ProcessProbe.state == 'BATTERY FAILURE')).all()
    count = 1
    for probe in offline_probes:
        # print(dir(probe))
        # print probe.last_heartbeat
        # break
        data = {}
        data['id'] = count
        data['probe_id'] = probe.probe_id
        data['probe_state'] = probe.state
        # data['config_name'] = probe.config_name
        data['last_probe_message'] = reformat_time(probe.last_probe_message)
        # data['shutdown_time'] = probe.shutdown_time or ''
        dump.append(data)
        count += 1
    return dump


@api.route('/offline_devices')
class OfflineDevices(Resource):
    """docstring for OfflineDevices"""

    # @api.doc(security='apikey')
    # @token_required
    def get(self):
        response = offline_probes(Probes)
        return {'data': response}


if __name__ == '__main__':
    app.run(debug=True)
