from kafka import KafkaConsumer
from elasticsearch import Elasticsearch
import json
import time
import signal
import collections
from hello import app

# print str(app.config['KAFKA_NODE'])

consumer = KafkaConsumer(bootstrap_servers=str(app.config['KAFKA_NODE']))
topic_poweron='IPMessage'
topic_message='gritServer'
topic_poweroff='IP_Message'
consumer.subscribe([topic_poweron, topic_poweroff, topic_message])

es_host = {"host": "localhost", "port":9200}
es = Elasticsearch(hosts= [es_host])
index_name = "cmdctrl"
type_name = "control"

for message in consumer:#repeats the following, for every item (in this case, message) in consumer.sudo systemctl start logstash.service
    message_value = json.loads(message.value)#converts the message from a string to a python dictionary
    description = message_value.get("description")#Gets messages that have the description key in them.
    gritServer_info = message_value.has_key("master")
    master = message_value.get("master")
    shutdown_complete = message_value.get("shutdown_complete")

    grit_time_data = {}
    ip_time_data = {}

    if description == "ipLog":#Done to identify IPMessage topics...
        data = message_value.get("data")#...get IP, lanIP and time information.
        data.update({"probeId" : message_value.get("probeId"), "status" : "PROBE ON", "shutdown_complete" : "false", "description" : "LIVE"})
        ipLog_time = data.get("time")
        ip_time_data.update({"probeId" : message_value.get("probeId"), "time" : ipLog_time})
        es.index(index=index_name,doc_type=type_name, body=data)

    if gritServer_info:
        current_time = master.get("time")
        grit_id = master.get("id")
        grit_time_data.update({"time" : current_time})

    if description != "ipLog" and gritServer_info == False:
        shutdown_message = message_value.get("description")
        shutdown_data = message_value.get("data")
        probeId = message_value.get("probeId")
        shutdown_complete = shutdown_data.get("shutdown_complete")
        shutdown_data.update({"description" : shutdown_message, "probeId" : probeId})
        if shutdown_complete == True:
            shutdown_data.update({"status" : "PROBE OFF"})
        else:
            shutdown_data.update({"status" : "PROBE OFFLINE"})
        es.index(index=index_name,doc_type=type_name, body=shutdown_data)
