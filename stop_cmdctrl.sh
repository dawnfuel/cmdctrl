#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# stop all kafka processes
exec ps -ef | grep kafka | cut -c 10-15 | xargs kill -9
# stop elasticsearch
# exec ps -ef | grep elasticsearch | cut -c 10-15 | xargs sudo kill -9

kafka_dir=$HOME"/Applications/kafka_2.11-0.9.0.0"
cmdctrl_dir=$HOME"/Documents/REPOS/cmdctrl"
# echo $kafka_dir
echo "Stopping ..."

# stop zookeeper
# $kafka_dir"/bin/zookeeper-server-stop"
# stop kafka
# $kafka_dir"/bin/kafka-server-stop"

# stop the producer
exec ps -ef | grep "python producer.py" | cut -c 10-15 | xargs kill -9
# stop the consumer
# exec ps -ef | grep "python esNew.py" | cut -c 10-15 | xargs sudo kill -9

exit 0
exit 0
