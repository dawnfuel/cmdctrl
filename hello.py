#!/usr/bin/env python
import os
import re
import json
import pprint
import logging
import threading
from threading import Timer

import bcrypt
import elasticsearch
import jinja2, itertools
from datetime import timedelta
from kafka import KafkaConsumer
from pymongo import MongoClient
from elasticsearch import Elasticsearch
from flask_sqlalchemy import SQLAlchemy
from flask_environments import Environments
from flask_socketio import SocketIO, emit, send
from flask import Flask, render_template, redirect, url_for, request, session
#from sqlalchemy import DateTime
#from sqlalchemy import exists

app = Flask(__name__, static_url_path='/static')
app.secret_key = os.urandom(12)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@localhost/cmdctrl'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

sqldb = SQLAlchemy(app)

socketio = SocketIO(app)
env = Environments(app)
env.from_object('config')
print app.config

kafkanode=str(app.config['KAFKA_NODE'])
topic_poweron='IPMessage'
topic_message='gritServer'
topic_poweroff='IP_Message'

es_host = {"host": "localhost", "port":9200}
es = Elasticsearch(hosts= [es_host])
index_name = "cmdctrl"
type_name = "control"

client = MongoClient(str(app.config['MONGO_DATABASE']))
db = client['lift-mongo-app']
print db
probes = db['probes']
configurations = db['configurations']
sources = db['powersources']
consumers = db['consumer_lines']
users = db['user.users']
print
probe_table = ""

upSinceDict = {}

for probe in probes.find():
    probe_table += json.dumps(str(probe))
    try:
        t_body = {"size":1,"query":{"bool":{"must":[{"match_phrase":{"probeId":probe["probeName"]}}]}}}
        t_result = es.search(index = index_name, size=1, body=t_body)
        t_resultHits = t_result.get('hits').get('hits')
        if t_resultHits: # Continues if t_searchRes is not empty
        	t_searchRes = t_resultHits[0].get('_source').get('time')
        	upSinceDict.update({probe["probeName"] : t_searchRes})
    except elasticsearch.exceptions.NotFoundError:
        print "ES Index Not Found"

probe_keyset = probes.find_one().keys()

# generate html table for db collection probes
health_params = ["last_message","up_since"]
health_params_set = [("last_message","time"),("up_since","time")]

f = open('templates/content.html', 'w')
f.write("")
f.write("<div>\nToggle column:\n")
column_num = 0

for probe_keys in probe_keyset:
    output_string = "      <a class=\"toggle-vis\" data-column="+str(column_num)+">"+str(probe_keys)+"    |    "+"</a>\n"
    f.write(output_string)
    column_num += 1
for param in health_params :
    output_string = "      <a class=\"toggle-vis\" data-column="+str(column_num)+">"+str(param)+"    |    "+"</a>\n"
    f.write(output_string)
    column_num += 1


f.write("</div>\n")
f.write("<table id=\"probelist\" border=\"1\" class=\"display\" cellspacing=\"0\" width=\"100%\">\n")
f.write("  <thead>\n")
f.write("    <tr>\n")
for probe_keys in probe_keyset:
	output_string = "      <th>"+str(probe_keys)+"</th>\n"
	f.write(output_string)
for param in health_params :
	output_string = "      <th>"+str(param)+"</th>\n"
	f.write(output_string)
f.write("    </tr>\n")
f.write("  </thead>\n")
f.write("  <tbody>\n")
f.write("  {% for probe in probes %}\n")
f.write("    <tr>\n")
for probe_keys in probe_keyset:
    if probe_keys == "probeName":
        f.write("      <td class=\"c1\" ><a href=\"history/{{ probe.probeName }}\">{{probe."+probe_keys+"}}</a></td>\n")
    elif probe_keys == "Channels":
	f.write("      <td class=\"c1\" >Too much data to display</td>\n")
    else :
	f.write("      <td class=\"c1\" >{{probe."+probe_keys+"}}</td>\n")
for param in health_params :
	output_string = "      <td id=\""+param+"{{ probe.probeName | remove_colon }}"+"\"></td>\n"
	f.write(output_string)
f.write("    </tr>\n")
f.write("  {% endfor %}\n")
f.write("  </tbody>\n")
f.write("</table>\n")
f.close()
counter = 0
def hello():
    counter =+ 1
    print "hello world "+str(counter)


class ProcessProbe(sqldb.Model):
    id = sqldb.Column(sqldb.Integer, primary_key=True,autoincrement=True, unique = True)
    probe_id = sqldb.Column(sqldb.String(50), unique=True)
    last_message = sqldb.Column(sqldb.DateTime())
    on_since = sqldb.Column(sqldb.DateTime())
    shutdown = sqldb.Column(sqldb.DateTime())
    # last_heartbeat = sqldb.Column(sqldb.DateTime())
    # timeout = sqldb.Column(sqldb.Interval(),default=timedelta(seconds=5))
    timeout = sqldb.Column(sqldb.Integer(),default=5)
    state = sqldb.Column(sqldb.String(12))
    #"ON/OFF/OFF BATT LOW/SHUTDOWN/OFFLINE"
    avg_message_frequency = sqldb.Column(sqldb.Integer())
    avg_heartbeat_frequency = sqldb.Column(sqldb.Integer())

    def continuousUpdate(self, contractId):
        print 'hello, world new'
        t = threading.Timer(self.timeout, self.continuousUpdate, [contractId],{} )
        # t = threading.Timer(4, continuousUpdate)
        t.start()
        # t.join()
        print 'Terminating'

    #daemon = True
    # def timeout(self):
    #     print "probe timeout"
    def __init__(self,probe_id):
        Thread.__init__(self)
        self.probe_id = probe_id
        self.continuousUpdate(123)

    #     print "probe : "+probe_id+" countdown set at "+str(countdown)+" seconds"
    #     # self.lock = lock
    #     # self.state = state

    # def run(self):
    #     print "running"
sqldb.create_all()

def repeat_to_length(string_to_expand, length):
    return (string_to_expand * ((length/len(string_to_expand))+1))[:length]

def thread_debug():
    print " "+str(threading.active_count())+" threads active"

class Consumer(threading.Thread):
    #daemon = True

    def run(self):

        print "CONSUMER STARTING"
        consumer = KafkaConsumer(bootstrap_servers=kafkanode)
        consumer.subscribe([topic_poweron,topic_poweroff,topic_message])
        print "CONSUMER LIVE"
        while True :

            for message in consumer:
                try:
                    thread_debug()
                    j = json.loads(message.value)
                    if 'description' in j:
                        #print "IP_Message topic format"
                        probe_id = j['probeId']
                        time = j['data']['time']
                        probe = ProcessProbe.query.filter_by(probe_id=probe_id).first()
                        # probe = Session.query(exists().where(ProcessProbe.probe_id=probe_id)).scalar()
                        print probe
                        if not probe :
                            print "creating probe "+probe_id
                            probe = ProcessProbe(probe_id=probe_id)
                        if j['description'] == "ipLog" :
                            # timertest = Timer(1.0,hello)
                            # timertest.start()
                            # probeobjects = ProcessProbe.objects.filter(probe_id = probe_id)
                            # for probeobject in probeobjects:
                                # print probeobject

                            #ProcessProbe(probe_id,1.0)

                            probe.on_since = time
                            probe.state = "ON"
                            print "probe : "+probe_id+" INITIALIZED @"+time
                        elif j['description'] == "shutdown" :
                            if j['data']['shutdown_complete'] == True :
                                print "probe : "+probe_id+" SHUTDOWN COMPLETE @"+time
                                probe.shutdown = time
                                probe.state = "OFF"
                            else :
                                print "probe : "+probe_id+" SHUTDOWN INITIALIZED @"+time
                                probe.shutdown = time
                                probe.state = "SHUTDOWN"
                        else :
                            print "probe : "+probe_id+" UNKNOWN SHUTDOWN MODE @"+time

                        sqldb.session.add(probe)
                        try:
                            sqldb.session.commit()
                        except Exception, e:
                            print e
                        print "\ndata :"
                        pprint.pprint(message.value)
                        socketio.emit('my response', message.value ,namespace='/kstream')
                    elif 'data' in j :
                        data = j['data'][0]
                        probe_id = data['id']
                        time = data['time']
                        probe = ProcessProbe.query.filter_by(probe_id=probe_id).first()
                        # probe = Session.query(exists().where(ProcessProbe.probe_id=probe_id)).scalar()
                        print probe
                        if not probe :
                            print "creating probe "+probe_id
                            probe = ProcessProbe(probe_id=probe_id)
                        probe.state = "ON"
                        probe.last_message = time
                        sqldb.session.add(probe)
                        try:
                            sqldb.session.commit()
                        except Exception, e:
                            print e
                        #print "gritServer topic format"
                        print "probe : "+probe_id+" ALIVE @"+time
                        print "\ndata :"
                        pprint.pprint(message.value)
                        socketio.emit('my response', message.value ,namespace='/kstream')
                    else :
                        print "Unknown Message Format"
                        print message.value
                        #socketio.emit('my response', message.value ,namespace='/kstream')
                    rows, columns = os.popen('stty size', 'r').read().split()
                    print repeat_to_length("_",int(columns))
                    print
                except Exception, e:
                    raise
                    print e
                    print "Bad Message Format"
                    try:
                        print message.value
                        print json.dumps(message.value, indent=4, sort_keys=True)
                    except Exception, e:
                        print message.value
                    print
threads = [
    Consumer()
]

for t in threads:
    t.start()

@app.template_filter('remove_colon')
def remove_colon(s):
    return re.sub(':','',s)

@app.route("/params", strict_slashes=False)
def measurement_params():
    return json.dumps(health_params_set)

@app.route("/history/<probeName>", strict_slashes=False)
def elastic_restAPI(probeName):
	body = {"size":20,"query":{"bool":{"must":[{"match_phrase":{"probeId":probeName}}]}}, "sort": [{"time": {"order": "desc"}}]}
	result = es.search(index = index_name, size=20, body=body)

	resultHits = result.get('hits').get('hits')
	tableData = []
	for record in resultHits:
		searchRes = record.get('_source')
		tableHead = searchRes.keys()
		tableData.append(searchRes)
	return render_template('history.html', tableData=(tableData))

# route for handling the login page logic
@app.route('/login', methods=['GET', 'POST'])
def login():
    usermail = request.form['username']
    userpass = request.form['password']
    error = None
    if request.method == 'POST':
        result = users.find_one({"email": usermail})
        if result:
            passwordHash = result.get('password')
            print
            if passwordHash != "":
                encoded_hash = passwordHash.encode("utf-8")
                if bcrypt.hashpw(userpass.encode("utf-8"), encoded_hash) == encoded_hash:
                    session['logged_in'] = True
        else:
            error = 'Invalid Credentials. Please try again.'
        return index()
    return render_template('login.html', error=error)

@app.route("/")
def index():
    if not session.get('logged_in'):
        return render_template('login.html')
    elif session['logged_in'] == False:
        return login()
    else:
        return render_template('index.html', probes=probes.find(), upSinceDict=upSinceDict, es_host=es_host)

@app.route("/logout")
def logout():
    session['logged_in'] = False
    return index()

if __name__ == "__main__":
    logging.basicConfig(
        format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
        level=logging.INFO
        )
    socketio.run(app)
    while True:
        pass
