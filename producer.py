#!/usr/bin/env python
import sys
import json
import time
import random
from threading import Thread, Event
from datetime import datetime
from kafka import KafkaProducer
from producer_utils import probes_lite

producer = KafkaProducer(bootstrap_servers='localhost:9092')
topics = ['gritServer', 'IP_Message', 'tamperReport']
# topics = ['tamperReport']
# topics = ['notifications']

def lookup_topic(topic):
  currentTime = str(datetime.now())
  probeId = random.choice(probes_lite)
  message_dict = {
# 	'notifications': (0, ['hello']),
      'gritServer': (0, [{"data": [{"current": [2047.0, 2047.0, 2047.0, 2047.0, 2047.0, 2047.0, 2047.0, 2047.0],
                                    "voltage": [230.0, 230.0, 230.0, 230.0, 230.0, 230.0, 230.0, 230.0],
                                    "costSinceLast": {"MUST4024C": 0.0, "PERKINSP40P3": 1.9065821170806885, "EKODISCO": 5.7129667147076919},
                                    "time": currentTime, "energyTodaySource": {
          "MUST4024C": 152.34577905887178, "PERKINSP40P3": 228.51866858830766, "EKODISCO": 228.51866858830766},
          "timeTodaySource": {"MUST4024C": 0.001412283049689399, "PERKINSP40P3": 0.001412283049689399,
                              "EKODISCO": 0.001412283049689399}, "Type": "1", "id": probeId, "costTodaySource": {
          "MUST4024C": 0.0, "PERKINSP40P3": 1.9065821170806885, "EKODISCO": 5.7129667147076919},
          "powerfactor": [0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8]}],
          "master": {"id": probeId, "time": currentTime}}
      ]),
      'IP_Message': (random.randrange(0, 2), [
          {"probeId": probeId, "data": {"IP": "41.217.110.12", "lanIP": "192.168.2.41", "time": currentTime},
           "description": "ipLog"},
          {"description": "shutdown", "probeId": probeId,
           "data": {"IP": "41.217.110.12", "lanIP": "192.168.2.41", "time": currentTime,
                    "shutdown_complete": bool(random.getrandbits(1))}}
      ]),

      'tamperReport': (0, [{"id": probeId, "report": [
          {"sourceName": "", "sourceChannel": 0, "state": bool(random.getrandbits(1)), "changed": bool(random.getrandbits(1))},
          {"sourceName": "", "sourceChannel": 1, "state": bool(random.getrandbits(1)), "changed": bool(random.getrandbits(1))},
          {"sourceName": "", "sourceChannel": 2, "state": bool(random.getrandbits(1)), "changed": bool(random.getrandbits(1))},
          {"sourceName": "", "sourceChannel": 3, "state": bool(random.getrandbits(1)), "changed": bool(random.getrandbits(1))},
          {"sourceName": "", "sourceChannel": 4, "state": bool(random.getrandbits(1)), "changed": bool(random.getrandbits(1))},
          {"sourceName": "PHCN", "sourceChannel": 5, "state": bool(random.getrandbits(1)), "changed": bool(random.getrandbits(1))},
          {"sourceName": "PHCN", "sourceChannel": 6, "state": bool(random.getrandbits(1)), "changed": bool(random.getrandbits(1))},
          {"sourceName": "PHCN", "sourceChannel": 7, "state": bool(random.getrandbits(1)), "changed": bool(random.getrandbits(1))}]}
      ])
  }
  return message_dict[topic]


def messagesQueue(topic, message_event):
  while message_event.is_set():
    (msg_idx, messages) = lookup_topic(topic)
    print msg_idx
    print json.dumps(messages[msg_idx]) + "\n"
    producer.send(topic, json.dumps(messages[msg_idx]))
    delay = random.randrange(2, 5)
    time.sleep(delay)


# Create three threads as follows
def create_threads(message_event, topics):
  producer_threads = []
  for topic in topics:
    t = Thread(target=messagesQueue, args=(topic, message_event,))
    producer_threads.append(t)
  return producer_threads


def main():
  message_event = Event()
  message_event.set()
  producer_threads = create_threads(message_event, topics)
  print producer_threads
  try:
    for thread in producer_threads:
      thread.daemon = True
      thread.start()
  except Exception, e:
    print e
    print "Error: unable to start thread"

  try:
    while 1:
      time.sleep(0.01)
  except KeyboardInterrupt:
    print 'Shutting Down Threads'
    message_event.clear()
    for thread in producer_threads:
      thread.join()
    print "Closing producer.py"
    sys.exit(0)


if __name__ == '__main__':
  main()
