#!/usr/bin/env python
from pymongo import MongoClient


client = MongoClient('mongodb://localhost:27017/')
db = client['lift-mongo-app']
probes = db['probes']
configurations = db['configurations']


def get_config_name(probe):
    try:
        configId = probe['Channels'][0].get('ConfigID_FK')
        get_config = configurations.find_one({'_id': configId})
        if probe['probeName'] == 'B8:27:EB:26:8F:40':
            print '\n'
            print "probe ", probe
            print '\n'
            print "configId ", configId
            print '\n'
            print "get_config ", get_config
            print '\n'
            print 'configurationName ', get_config['configurationName']
            print '\n'
        if isinstance(get_config, dict):
            config_name = get_config['configurationName']
            print 'config_name = ', config_name
        else:
            config_name = 'Not_Defined'
        return config_name
    except Exception as e:
        # logger.exception(e)
        print e
        config_name = 'Not Found'
        return config_name


def get_all_config_names(probes):
    for probe in probes.find():
        print get_config_name(probe)


get_all_config_names(probes)
