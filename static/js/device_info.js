$(document).ready(function(){
    var namespace = '/info_stream';
    var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + namespace);
    var client_message = String(new Date());
    socket.emit("info_connected", client_message);
    console.log("[x] Connected at        : " + client_message);

    socket.on('info_channel', function(message) {
        console.log("[x] Received    : " + message);
    });

    socket.on('state_response', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
       console.log(msg);
    });

    socket.on('msgs_per_hour', function(msg) {
        $('td#total_messages_in_the_last_hour' + msg['id']).empty().append(msg['msg_num']);
        console.log(msg);
    });

    socket.on('msgs_per_day', function(msg) {
        $('td#total_messages_in_the_last_day' + msg['id']).empty().append(msg['msg_num']);
        console.log(msg);
    });

    socket.on('probe_on', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
        $('td#restart_cause' + msg['id']).empty().append(msg['last_state']);
        $('td#probe_state' + msg['id']).css('background-color', '#00FF00');
        console.log(msg);
    });

    socket.on('probe_on_data', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#last_power' + msg['id']).empty().append(msg['last_power']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
        $('td#restart_cause' + msg['id']).empty().append(msg['last_state']);
        $('td#probe_state' + msg['id']).css('background-color', '#00FF00');
        console.log(msg);
    });

    socket.on('probe_off', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
        $('td#probe_state' + msg['id']).css('background-color', '#0000FF');
        console.log(msg);
    });

    socket.on('shutdown_initialised', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
        $('td#probe_state' + msg['id']).css('background-color', '#00FFFF');
        console.log(msg);
    });

    socket.on('offline', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
        $('td#probe_state' + msg['id']).css('background-color', '#FF0000');
        console.log(msg);
    });

    socket.on('state_load', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#probe_state' + msg['id']).css('background-color', '#eb42f4');
        console.log(msg);
    });

    socket.on('battery_failure', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
        $('td#probe_state' + msg['id']).css('background-color', '#FFFF00');
        console.log(msg);
    });
});
