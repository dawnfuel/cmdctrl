$(document).ready(function() {
    var table = $('#probelist').DataTable({
        // "scrollY": "200px",
        'stripe': true,
        "paging": false,
        'background-color': '#78866B'
    });

    var toggler = $('a.toggle-vis').on('click', function(e) {
        e.preventDefault(); // Get the column API object
        var column = table.column($(this).attr('data-column')); // Toggle the visibility
        column.visible(!column.visible());
    });

    var namespace = '/kstream';
    var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + namespace);
    var client_message = String(new Date());
    socket.emit("connected", client_message);
    console.log("[x] Connected at        : " + client_message);

    socket.on('server_channel', function(message) {
        console.log("[x] Received    : " + message);
    });

    socket.on('state_response', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_probe_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#last_heartbeat' + msg['id']).empty().append(msg['socket_time']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
        console.log(msg);
    });

    socket.on('msgs_per_hour', function(msg) {
        $('td#messages_per_hour' + msg['id']).empty().append(msg['msg_num']);
        console.log(msg);
    });

    socket.on('msgs_per_day', function(msg) {
        $('td#messages_per_day' + msg['id']).empty().append(msg['msg_num']);
        console.log(msg);
    });

    socket.on('probe_on', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_probe_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#last_heartbeat' + msg['id']).empty().append(msg['socket_time']);
        $('td#last_restart_time' + msg['id']).empty().append(msg['restart_time']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
        $('td#restart_cause' + msg['id']).empty().append(msg['last_state']);
        $('td#probe_state' + msg['id']).css('background-color', '#00FF00');
        console.log(msg);
    });

    socket.on('probe_on_data', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_probe_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#last_heartbeat' + msg['id']).empty().append(msg['socket_time']);
        $('td#last_restart_time' + msg['id']).empty().append(msg['restart_time']);
        $('td#last_power' + msg['id']).empty().append(msg['last_power']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
        $('td#restart_cause' + msg['id']).empty().append(msg['last_state']);
        $('td#probe_state' + msg['id']).css('background-color', '#00FF00');
        console.log(msg);
    });

    socket.on('probe_off', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_probe_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#last_heartbeat' + msg['id']).empty().append(msg['socket_time']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
        $('td#probe_state' + msg['id']).css('background-color', '#0000FF');
        console.log(msg);
    });

    socket.on('shutdown_initialised', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_probe_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#last_heartbeat' + msg['id']).empty().append(msg['socket_time']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
        $('td#probe_state' + msg['id']).css('background-color', '#00FFFF');
        console.log(msg);
    });

    socket.on('offline', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_probe_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#last_heartbeat' + msg['id']).empty().append(msg['socket_time']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
        $('td#probe_state' + msg['id']).css('background-color', '#FF0000');
        console.log(msg);
    });

    socket.on('state_load', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#probe_state' + msg['id']).css('background-color', '#eb42f4');
        console.log(msg);
    });

    socket.on('battery_failure', function(msg) {
        $('td#probe_state' + msg['id']).empty().append(msg['state']);
        $('td#last_probe_message' + msg['id']).empty().append(msg['probe_time']);
        $('td#last_heartbeat' + msg['id']).empty().append(msg['socket_time']);
        $('td#up_since' + msg['id']).empty().append(msg['time_up']);
        $('td#probe_state' + msg['id']).css('background-color', '#FFFF00');
        console.log(msg);
    });
});
