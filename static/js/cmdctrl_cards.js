$(document).ready(function() {
    var active_probes = $("<div></div>").attr("id", "active_probes");
    var inactive_probes = $("<div></div>").attr("id", "inactive_probes");
    // var active_probes_text = $("<small></small>").text("Active Probes").addClass("text-muted");
    // var inactive_probes_text = $("<small></small>").text("Inactive Probes").addClass("text-muted");
    var active_probes_text = $("<small></small>").text("Active Probes").addClass("h6 container_text");
    var inactive_probes_text = $("<small></small>").text("Inactive Probes").addClass("h6 container_text");
    var active_probe_rows = $("<div></div>").addClass("row").attr("id", "active_rows");
    var inactive_probe_rows = $("<div></div>").addClass("row").attr("id", "inactive_rows");

    $("html").addClass("page_body");
    $("body").addClass("page_body");
    $("#uninitialized_probes").prepend(active_probes, inactive_probes);
    $(active_probes).addClass('container-fluid container_color').append(active_probes_text, active_probe_rows);
    $(inactive_probes).addClass('container-fluid container_color').append(inactive_probes_text, inactive_probe_rows);

    $(".card").click(function() {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    var namespace = '/card_stream';
    var card_socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + namespace);
    var connection_message = String(new Date());

    card_socket.emit('card_connected', connection_message);
    console.log('[x] Card View at\t\t:' + connection_message);

    card_socket.on('card_channel', function(message) {
        console.log("[x] Received\t\t: " + message);
    });

    card_socket.on('active_state', function(msg) {
        if ($('#probe' + msg['id']).parent('div#active_rows').length){
            $('#last_probe_message' + msg['id']).empty().append(msg['probe_time']);
            $('#probe_state' + msg['id']).empty().append(msg['state']);
            $('#probe' + msg['id']).css('background-color', '#00C853');
            console.log('Active Probe ', msg);
        }
        else{
            $('#probe' + msg['id']).fadeOut(200);
            $('#probe' + msg['id']).removeClass("card-inverse").fadeIn(200).appendTo(active_probe_rows);
            $('#last_probe_message' + msg['id']).empty().append(msg['probe_time']);
            $('#probe_state' + msg['id']).empty().append(msg['state']);
            $('#probe' + msg['id']).css('background-color', '#00C853');
            console.log('Active Probe ', msg);
        }
    });

    card_socket.on('down_state', function(msg) {
        if ($('#probe' + msg['id']).parent('div#inactive_rows').length){
            $('#last_probe_message' + msg['id']).empty().append(msg['probe_time']);
            $('#probe_state' + msg['id']).empty().append(msg['state'])
            $('#probe' + msg['id']).css('background-color', '#FF1744');
            console.log('Inactive Probe ', msg);
        }
        else{
            $('#probe' + msg['id']).removeClass("card-inverse").appendTo(inactive_probe_rows);
            $('#last_probe_message' + msg['id']).empty().append(msg['probe_time']);
            $('#probe_state' + msg['id']).empty().append(msg['state'])
            $('#probe' + msg['id']).css('background-color', '#FF1744');
            console.log('Inactive Probe ', msg);
        }
    });

    card_socket.on('state_response', function(msg) {
        console.log('Wrong Message Format')
    });
});
// animations to use
// animated fadeOut for transition from active_state to down state and vice-versa
// animated hinge when user clicks to go to another page
